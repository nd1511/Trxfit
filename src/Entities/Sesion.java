/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author Nico
 */
public class Sesion {
 
    
    String Clase,Entrenador, Fecha;
    int idSesion,Horai,Horaf;

    public Sesion(int idSesion,String Clase, String Entrenador, String Fecha, int Horai, int Horaf) {
        
        this.idSesion= idSesion; 
        this.Clase = Clase;
        this.Entrenador = Entrenador;
        this.Fecha = Fecha;
        this.Horai = Horai;
        this.Horaf = Horaf;
    }

    public String getClase() {
        return Clase;
    }

    public void setClase(String Clase) {
        this.Clase = Clase;
    }

    public String getEntrenador() {
        return Entrenador;
    }

    public void setEntrenador(String Entrenador) {
        this.Entrenador = Entrenador;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }

    public int getHorai() {
        return Horai;
    }

    public void setHorai(int Horai) {
        this.Horai = Horai;
    }

    public int getHoraf() {
        return Horaf;
    }

    public void setHoraf(int Horaf) {
        this.Horaf = Horaf;
    }

    public int getIdSesion() {
        return idSesion;
    }

    public void setIdSesion(int idSesion) {
        this.idSesion = idSesion;
    }

    @Override
    public String toString() {
        return "Sesion{" + "Clase=" + Clase + ", Entrenador=" + Entrenador + ", Fecha=" + Fecha + ", idSesion=" + idSesion + ", Horai=" + Horai + ", Horaf=" + Horaf + '}';
    }


}
