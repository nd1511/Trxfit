package Entities;

/**
 *
 * @author Yiyisman
 */
public class Reserva {
    int Idreserva, IdCLiente,IdSesion;

    public Reserva(int Idreserva, int IdCLiente, int IdSesion, String fechaReserva) {
        this.Idreserva = Idreserva;
        this.IdCLiente = IdCLiente;
        this.IdSesion = IdSesion;
        this.fechaReserva = fechaReserva;
    }
    
    public Reserva(int Idreserva) {
        this.Idreserva = Idreserva;
    }

    public int getIdCLiente() {
        return IdCLiente;
    }

    public void setIdCLiente(int IdCLiente) {
        this.IdCLiente = IdCLiente;
    }

    public int getIdSesion() {
        return IdSesion;
    }

    public void setIdSesion(int IdSesion) {
        this.IdSesion = IdSesion;
    }

    public String getFechaReserva() {
        return fechaReserva;
    }

    public void setFechaReserva(String fechaReserva) {
        this.fechaReserva = fechaReserva;
    }
    String fechaReserva;

    

    public int getIdreserva() {
        return Idreserva;
    }

    public void setIdreserva(int Idreserva) {
        this.Idreserva = Idreserva;
    }
    
}