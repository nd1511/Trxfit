package Entities;

/**
 *
 * @author Nico
 */
public class Pago {
    String Nombre;
    int idPago,idCliente; 
    String Documento;
    Float valor;
    String estado;

    public Pago(String Nombre, int idPago, int idCliente, String Documento, Float valor, String estado) {
        this.Nombre = Nombre;
        this.idPago = idPago;
        this.idCliente = idCliente;
        this.Documento = Documento;
        this.valor = valor;
        this.estado = estado;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }
    
    public int getIdPago() {
        return idPago;
    }

    public void setIdPago(int idPago) {
        this.idPago = idPago;
    }

    
    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getDocumento() {
        return Documento;
    }

    public void setDocumento(String Documento) {
        this.Documento = Documento;
    }

    public Float getValor() {
        return valor;
    }

    public void setValor(Float valor) {
        this.valor = valor;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Pago{" + "Nombre=" + Nombre + ", Documento=" + Documento + ", valor=" + valor + ", estado=" + estado + '}';
    }
    
}