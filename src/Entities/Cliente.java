/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author Nico
 */
public class Cliente {
    
    String Nombre,Documento,Login,Contraseña;
    int idCliente; 

    public Cliente(String Nombre, String Documento, String Login, String Contraseña, int idCliente) {
        this.Nombre = Nombre;
        this.Documento = Documento;
        this.Login = Login;
        this.Contraseña = Contraseña;
        this.idCliente = idCliente;
    }
    
    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getDocumento() {
        return Documento;
    }

    public void setDocumento(String Documento) {
        this.Documento = Documento;
    }

    public String getLogin() {
        return Login;
    }

    public void setLogin(String Login) {
        this.Login = Login;
    }

    public String getContraseña() {
        return Contraseña;
    }

    public void setContraseña(String Contraseña) {
        this.Contraseña = Contraseña;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    
    @Override
    public String toString() {
        return "Cliente{" + "Nombre=" + Nombre + ", Documento=" + Documento + ", Login=" + Login + ", Contrase\u00f1a=" + Contraseña + '}';
    }
   
}
