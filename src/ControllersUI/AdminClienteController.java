/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersUI;

import Conexion.ReturnEntities;
import Entities.Cliente;
import Entities.Pago;
import Entities.Sesion;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author Nico
 */
public class AdminClienteController implements Initializable {

@FXML
    private Button btn_search;

    @FXML
    private TableView<Pago> tb_clientes;

    @FXML
    private ImageView img_worker;

    @FXML
    private Button btn_stateSwitch;

    @FXML
    private ImageView img_class;

    @FXML
    private ImageView img_client;

    @FXML
    private TableColumn<Pago, String> c_id;

    @FXML
    private TableColumn<Pago, String> c_valor;

    @FXML
    private TableColumn<Pago, String> c_name;

    @FXML
    private TableColumn<Pago, String> c_payState;

    @FXML
    private TextField txt_filtro;

    @FXML
    private ImageView img_back;
    
    ReturnEntities Re= new ReturnEntities(); 
    private ObservableList<Pago> list;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
    }    

    @FXML
    private void ActionClicked(MouseEvent event) {
        
        if(event.getTarget()== img_worker){      
           
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/AdminWorker.fxml"));
            this.AbrirFXML(fxmlLoader,event); 
             
         }else if(event.getTarget()== img_class){
             
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/AdminClases.fxml"));
            this.AbrirFXML(fxmlLoader,event);
             
         }else if(event.getTarget()== img_back){
             
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/MainAdmin.fxml"));
            this.AbrirFXML(fxmlLoader,event);
             
         }
    }
    
    
        private void AbrirFXML(FXMLLoader fxmlLoader, MouseEvent evento) {
        
         try {
                       
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initStyle(StageStyle.DECORATED);
            Scene scene = new Scene(root1);
            stage.setScene(scene);
            stage.show();
            stage.setResizable(false);
            ((Node)(evento.getSource())).getScene().getWindow().hide();
            
         } catch(Exception e) {
            
            e.printStackTrace();
        }
         
         

    }

    @FXML
    void filtroAction(ActionEvent event) {

        this.llenarTabla(); 
        
        
    }
    
    private void llenarTabla() {
       
                String docu = this.txt_filtro.getText(); 
        
        try{
           
             ArrayList<Pago> pagos = Re.ReturnPagos(docu); 
             
             list = FXCollections.observableArrayList(pagos);
             
            this.c_name.setCellValueFactory(new PropertyValueFactory<Pago, String>("Nombre"));
            this.c_id.setCellValueFactory(new PropertyValueFactory<Pago, String>("Documento"));
            this.c_valor.setCellValueFactory(new PropertyValueFactory<Pago, String>("valor"));
            this.c_payState.setCellValueFactory(new PropertyValueFactory<Pago, String>("estado"));
            this.tb_clientes.setItems(list);
             
        }catch(Exception e) {
            
            System.out.println(e);
        }
    }
    

    @FXML
    void StateAction(ActionEvent event) {
        
       Pago pg = this.tb_clientes.getSelectionModel().getSelectedItem(); 
       
       if(pg.getEstado().equals("No Pagado")){
           
           try{
               
               Re.ActualizarPagos(pg.getIdPago());
               Re.InsertarPagoCliente(pg.getIdCliente());
               this.llenarTabla();
               
           }catch(Exception e){
               System.out.println(e);
           }
            
       }else{
           
           Alert alert = new Alert(Alert.AlertType.INFORMATION,"Ya esta Pagado", ButtonType.CLOSE);
           alert.showAndWait(); 
       }
    }

}
