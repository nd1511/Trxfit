/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersUI;

import Entities.Clase;
import Entities.Sesion;
import Entities.Trabajador;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author Nico
 */

public class AdminClasesController implements Initializable {

    @FXML
    private ComboBox<Clase> cb_clases;

    @FXML
    private TableColumn<Sesion, String> c_hini;

    @FXML
    private TextField txt_hfin;

    @FXML
    private TableColumn<Sesion, String> c_clase;

    @FXML
    private TableColumn<Sesion, String> c_hfin;

    @FXML
    private TextField txt_cap;

    @FXML
    private ComboBox<Trabajador> cb_workers;

    @FXML
    private ImageView img_class;

    @FXML
    private TextField txt_value;

    @FXML
    private RadioButton rb_open;

    @FXML
    private TableColumn<Sesion, String> c_estado;

    @FXML
    private Tab cb_classes;

    @FXML
    private TableView<Sesion> tb_clases;

    @FXML
    private TextField txt_cod;

    @FXML
    private TextField txt_duracion;

    @FXML
    private RadioButton rb_finalizada;

    @FXML
    private TableColumn<Sesion, String> c_fecha;

    @FXML
    private ImageView img_worker;
    
    @FXML
    private ImageView img_back;

    @FXML
    private ImageView img_client;

    @FXML
    private TextField txt_hini;
    
    @FXML
    private Button btn_addSesion;

    @FXML
    private TableColumn<Sesion, String> c_worker;

    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void ActionClicked(MouseEvent event) {
        
          if(event.getTarget()== img_worker){      
           
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/AdminWorker.fxml"));
            this.AbrirFXML(fxmlLoader,event); 
             
         }else if(event.getTarget()== img_class){
             
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/AdminClases.fxml"));
            this.AbrirFXML(fxmlLoader,event);
             
         }else if(event.getTarget()== img_client){
             
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/AdminCliente.fxml"));
            this.AbrirFXML(fxmlLoader,event);
             
         }else if(event.getTarget()== img_back){
             
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/MainAdmin.fxml"));
            this.AbrirFXML(fxmlLoader,event);
             
         }else if(event.getTarget()== btn_addSesion){
             
             
             
         }
        
    }
    
    private void AbrirFXML(FXMLLoader fxmlLoader, MouseEvent evento) {
        
         try {
                       
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initStyle(StageStyle.DECORATED);
            Scene scene = new Scene(root1);
            stage.setScene(scene);
            stage.show();
            stage.setResizable(false);
            ((Node)(evento.getSource())).getScene().getWindow().hide();
            
         } catch(Exception e) {
            
            e.printStackTrace();
        }
    }
}
