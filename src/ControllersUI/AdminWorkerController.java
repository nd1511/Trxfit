/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersUI;

import Entities.Sesion;
import Entities.Trabajador;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author Nico
 */
public class AdminWorkerController implements Initializable {

    @FXML
    private ImageView img_class;
    @FXML
    private ImageView img_worker;
    @FXML
    private ImageView img_client;
    @FXML
    private ImageView img_back;
    @FXML
    private TableView<Trabajador> tb_clientes;
    @FXML
    private TableColumn<Trabajador, String> c_user;
    @FXML
    private TableColumn<Trabajador, String> c_id;
    @FXML
    private TableColumn<Trabajador, String> c_Hini;
    @FXML
    private TableColumn<Trabajador, String> c_Hfin;
    @FXML
    private TextField txt_filtro;
    @FXML
    private PasswordField txt_pass;
    @FXML
    private TextField txt_user;
    @FXML
    private TextField txt_name;
    @FXML
    private Button btn_addWorker;
    @FXML
    private Tab cb_classes;
    @FXML
    private TableView<Sesion> tb_sesionesAct;
    @FXML
    private TableColumn<Sesion, String> c_clase;
    @FXML
    private TableColumn<Sesion, String> c_cod;
    @FXML
    private TableColumn<Trabajador, String> c_Hinicio;
    @FXML
    private TableColumn<Trabajador, String> c_Hfinal;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void ActionClicked(MouseEvent event) {
        
         if(event.getTarget()== img_class){
             
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/AdminClases.fxml"));
            this.AbrirFXML(fxmlLoader,event);
             
         }else if(event.getTarget()== img_client){
             
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/AdminCliente.fxml"));
            this.AbrirFXML(fxmlLoader,event);
             
         }else if(event.getTarget()== img_back){
             
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/MainAdmin.fxml"));
            this.AbrirFXML(fxmlLoader,event);  
         }
    }
 
    private void AbrirFXML(FXMLLoader fxmlLoader, MouseEvent evento) {
        
         try {
                       
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initStyle(StageStyle.DECORATED);
            Scene scene = new Scene(root1);
            stage.setScene(scene);
            stage.show();
            stage.setResizable(false);
            ((Node)(evento.getSource())).getScene().getWindow().hide();
            
         } catch(Exception e) {
            
            e.printStackTrace();
        }
    }

}
