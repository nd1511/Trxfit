/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersUI;


import ControllersServicios.ClienteController;
import com.sun.javafx.stage.StageHelper;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author Nico
 */
public class SignInController implements Initializable {

    @FXML
    private ImageView img_signA;
    @FXML
    private Text txt_signA;
    @FXML
    private Text txt_signup;
    @FXML
    private ImageView img_sign;
    @FXML
    private Pane pane_admin;
    @FXML
    private TextField tf_admin;
    @FXML
    private Pane pane_user;
    @FXML
    private TextField tf_user;
    @FXML
    private ImageView btn_nextU;
    @FXML
    private ImageView btn_nextA;
    @FXML
    private Pane pane_ini;
    
    @FXML
    private PasswordField ps_usr;
    @FXML
    private PasswordField ps_admin;

    boolean accesoA=false; 
    
    ClienteController Cr = new ClienteController(); 
    /**
     * Initializes the controller class.
     */
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.pane_ini.toFront();
    }    

    @FXML
    private void ActionClicked(MouseEvent event) throws ClassNotFoundException, SQLException, IllegalAccessException, InstantiationException {
        
                 
        
         if(event.getTarget()== img_signA){
             
             this.pane_admin.toFront();
             
         }else if(event.getTarget()== img_sign){
             
             this.pane_user.toFront();
             
         }else if(event.getTarget()== btn_nextU){
            
            String nombre = this.tf_user.getText(); 
            String pass = this.ps_usr.getText(); 

            accesoA = Cr.ConfirmarUsuario(nombre,pass);
           

            if(accesoA){

                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/MainCliente.fxml"));
                this.AbrirFXML(fxmlLoader,event); 

            }else{

                Alert alert = new Alert(AlertType.ERROR,"Usuario ò Contraseña Invalido", ButtonType.CLOSE);
                alert.showAndWait();
                this.tf_user.setText("");
                this.ps_usr.setText("");
                
            }
  
        }else if(event.getTarget()== btn_nextA ){
             
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/MainAdmin.fxml"));
            this.AbrirFXML(fxmlLoader,event); 
           
        }
            
    }
    
    @FXML
    private void ActionTxtClicked(MouseEvent event) {
    
         if(event.getTarget()== txt_signup){
             
             this.pane_user.toFront();
             
             
         }else if(event.getTarget()== txt_signA){
             
             this.pane_admin.toFront();
         
         }
         
    }

    private void AbrirFXML(FXMLLoader fxmlLoader, MouseEvent evento) {
        
         try {
                       
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initStyle(StageStyle.DECORATED);
            Scene scene = new Scene(root1);
            stage.setScene(scene);
            stage.show();
            stage.setResizable(false);
            ((Node)(evento.getSource())).getScene().getWindow().hide();
            
         } catch(Exception e) {
            
            e.printStackTrace();
        }
    }
}
