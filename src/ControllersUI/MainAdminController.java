package ControllersUI;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class MainAdminController implements Initializable{


    @FXML
    private ImageView img_worker;

    @FXML
    private Text txt_worker;

    @FXML
    private ImageView img_class;

    @FXML
    private ImageView img_client;
    
    @FXML
    private Text txt_client;

    @FXML
    private Text txt_class;

    
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        
    }
    
    
    @FXML
    void ActionClicked(MouseEvent event) {

         if(event.getTarget()== img_worker){      
           
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/AdminWorker.fxml"));
            this.AbrirFXML(fxmlLoader,event); 
             
         }else if(event.getTarget()== img_class){
             
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/AdminClases.fxml"));
            this.AbrirFXML(fxmlLoader,event);
             
         }else if(event.getTarget()== img_client){
             
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/AdminCliente.fxml"));
            this.AbrirFXML(fxmlLoader,event);
             
         }
         
    }

    private void AbrirFXML(FXMLLoader fxmlLoader, MouseEvent evento) {
        
         try {
                       
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initStyle(StageStyle.DECORATED);
            Scene scene = new Scene(root1);
            stage.setScene(scene);
            stage.show();
            stage.setResizable(false);
            ((Node)(evento.getSource())).getScene().getWindow().hide();
            
         } catch(Exception e) {
            
            e.printStackTrace();
        }
    }


    


}
