/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersUI;

import Conexion.ReturnEntities;
import ControllersServicios.SesionController;
import Entities.Clase;
import Entities.Sesion;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author Nico
 */
public class MainClienteController implements Initializable {

    @FXML
    private Text txt_class;
    @FXML
    private Text txt_worker;
    @FXML
    private Text txt_client;
    @FXML
    private Text txt_client1;
    @FXML
    private TableView<Sesion> tb_clases;
    @FXML
    private TableColumn<Sesion, String> c_clase;
    @FXML
    private TableColumn<Sesion, String> c_worker;
    @FXML
    private TableColumn<Sesion, String> c_fecha;
    @FXML
    private TableColumn<Sesion, String> c_hini;
    @FXML
    private TableColumn<Sesion, String> c_hfin;
    @FXML
    private ImageView img_pay;
    @FXML
    private ImageView img_sesion;
    @FXML
    private ImageView img_res;
    @FXML
    private ImageView img_back;
    @FXML
    private ComboBox<String> cb_clases;
    @FXML
    private TextField txt_prof;
    @FXML
    private TextField txt_inicio;
    @FXML
    private TextField txt_fin;
    @FXML
    private Button btn_reservar;
    
     private ObservableList<Sesion> list;
     SesionController sc= new SesionController(); 

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        this.llenartb("Inicio"); 
        this.llenarComboBox();
        
        
    }    

    @FXML
    private void ActionClicked(MouseEvent event) {
        
        
        if(event.getTarget()== img_pay){      
           
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/ClientePagos.fxml"));
            this.AbrirFXML(fxmlLoader,event); 
             
         }else if(event.getTarget()== img_sesion){
             
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/ClienteSesiones.fxml"));
            this.AbrirFXML(fxmlLoader,event);
             
         }else if(event.getTarget()== img_back){
             
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/SignIn.fxml"));
            this.AbrirFXML(fxmlLoader,event);
             
         }
        
    }
    
    @FXML
    void filtradoCB(ActionEvent event) {

        String Class = this.cb_clases.getSelectionModel().getSelectedItem(); 
        this.llenartb(Class);
    }
    
    private void AbrirFXML(FXMLLoader fxmlLoader, MouseEvent evento) {
        
         try {
                       
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initStyle(StageStyle.DECORATED);
            Scene scene = new Scene(root1);
            stage.setScene(scene);
            stage.show();
            stage.setResizable(false);
            ((Node)(evento.getSource())).getScene().getWindow().hide();
            
         } catch(Exception e) {
            
            e.printStackTrace();
        }
    }

    private void llenartb(String clase) {
       
        try { 
            
            ArrayList<Sesion> sesiones =  sc.FillTableSesionesPorClase(clase); 
                                   
            list = FXCollections.observableArrayList(sesiones);

            this.c_clase.setCellValueFactory(new PropertyValueFactory<Sesion, String>("Clase"));
            this.c_worker.setCellValueFactory(new PropertyValueFactory<Sesion, String>("Entrenador"));
            this.c_fecha.setCellValueFactory(new PropertyValueFactory<Sesion, String>("Fecha"));
            this.c_hini.setCellValueFactory(new PropertyValueFactory<Sesion, String>("Horai"));
            this.c_hfin.setCellValueFactory(new PropertyValueFactory<Sesion, String>("Horaf"));
            this.tb_clases.setItems(list);
            
        } catch (Exception ex) {
            
            ex.printStackTrace();
        } 
        
    }
    
     private void llenarComboBox() {
        
         this.cb_clases.getItems().addAll(
            "Pilates",
            "Zumba"
        );    
        
     
    }
 
    
    @FXML
    void Reservar(ActionEvent event) {
        
        Sesion Se = this.tb_clases.getSelectionModel().getSelectedItem(); 
        System.out.println(Se.toString()); 
        
        sc.reservar(Se); 
    }
}
