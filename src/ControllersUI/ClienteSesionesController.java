/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersUI;

import Entities.Sesion;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author Nico
 */
public class ClienteSesionesController implements Initializable {

    @FXML
    private Text txt_class;
    @FXML
    private Text txt_worker;
    @FXML
    private Text txt_client;
    @FXML
    private ImageView img_pay;
    @FXML
    private ImageView img_sesion;
    @FXML
    private ImageView img_res;
    @FXML
    private Text txt_client1;
    @FXML
    private ImageView img_back;
    @FXML
    private TableView<Sesion> tb_clases;
    @FXML
    private TableColumn<Sesion, String> c_clase;
    @FXML
    private TableColumn<Sesion, String> c_worker;
    @FXML
    private TableColumn<Sesion, String> c_fecha;
    @FXML
    private TableColumn<Sesion, String> c_hini;
    @FXML
    private TableColumn<Sesion, String> c_hfin;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void ActionClicked(MouseEvent event) {
        
         if(event.getTarget()== img_pay){      
           
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/ClientePagos.fxml"));
            this.AbrirFXML(fxmlLoader,event); 
             
         }else if(event.getTarget()== img_res){
             
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/MainCliente.fxml"));
            this.AbrirFXML(fxmlLoader,event);
             
         }else if(event.getTarget()== img_back){
             
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Frames/SignIn.fxml"));
            this.AbrirFXML(fxmlLoader,event); 
         }
    }
    
     private void AbrirFXML(FXMLLoader fxmlLoader, MouseEvent evento) {
        
         try {
                       
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initStyle(StageStyle.DECORATED);
            Scene scene = new Scene(root1);
            stage.setScene(scene);
            stage.show();
            stage.setResizable(false);
            ((Node)(evento.getSource())).getScene().getWindow().hide();
            
         } catch(Exception e) {
            
            e.printStackTrace();
        }
    }
    
}
