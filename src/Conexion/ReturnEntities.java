/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexion;

import ControllersServicios.Unimportant;
import Entities.Cliente;
import Entities.Pago;
import Entities.Sesion;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author Nico
 */
public class ReturnEntities {
    
     
        static ConexionPost connSQL = new ConexionPost();
	static Statement s = null;
	static PreparedStatement ps = null;
	static int contador = 0;

	
              
        public ArrayList<Cliente> ReturnCliente()
                        throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException{
            ArrayList<Cliente> clientes = new ArrayList(); 
            try{
                
            Connection conn = connSQL.SetConeccion();
            
            java.sql.Statement st = conn.createStatement();
            String sql = "SELECT IdCliente,Login,Contraseña,Nombre,Documento From Cliente";
            
            ResultSet result = st.executeQuery(sql);
        
        
            while(result.next()){
            
            int id = result.getInt("IdCliente"); 
            String nombre= result.getString("Nombre");
            String docu= result.getString("Documento");
            String log= result.getString("Login");
            String pass= result.getString("Contraseña");
            
            Cliente Cli = new Cliente(nombre,docu,log,pass,id);
            
            clientes.add(Cli); 
                                   
            }
            result.close();
            st.close();
            conn.close();
            
            }
            catch(Exception e){
                System.out.println(e);
            }
            
            
            return clientes;   
        }
        
        
        public ArrayList<Sesion> ReturnSesion()
                        throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException{
            ArrayList<Sesion> array= new ArrayList(); 
            try{
            Connection conn = connSQL.SetConeccion();
            
            java.sql.Statement st = conn.createStatement();
            String sql = "SELECT idSesion,Clase,Nombre,Fecha,HoraInicio,HoraFin From Sesion inner join Horario on Horario.IdHorario = Sesion.IdHorario inner join Trabajador on Trabajador.IdTrabajador = Sesion.IdTrabajador Where Finalizada=false";
            
            ResultSet result = st.executeQuery(sql);
        
            Sesion se;
            while(result.next()){
            
            int id= result.getInt("idSesion");
            String Clase= result.getString("Clase");
            String Entrenador= result.getString("Nombre");
            String Fecha= result.getString("Fecha");
            int Horai= result.getInt("HoraInicio");
            int Horaf= result.getInt("HoraFin");
            
            se = new Sesion(id,Clase,Entrenador,Fecha,Horai, Horaf);
            
            array.add(se);
           
            }            
            
            result.close();
            st.close();
            conn.close();
            return array;
            
            }
            catch(Exception e){
                System.out.println(e);
            }
            
            return array;
        }
        
        public void InsertarReserva(int id,int idCliente, int IdSesion) 
                throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException
 {
          try{
          // Hago la conexi n
          Connection conn = connSQL.SetConeccion();
                        // Insertar
          ps = conn.prepareStatement("insert into Reserva(IdReserva,FechaReserva,IdCliente,IdSesion) values (?,current_date,?,?);");

                        ps.setInt(1, id);
                        ps.setInt (2, idCliente);
                        ps.setInt(3, IdSesion);

          contador = ps.executeUpdate();

                        System.out.println(contador + " Reserva insertada\n");

                        // Cierro el resulset, statement, pstatement
          ps.close();
                        conn.close();

        }catch(Exception e){
            System.out.println(e);
        }   
 }
        
        public ArrayList<Integer> IdReserva()
                        throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException{
            
            ArrayList<Integer> array= new ArrayList(); 
            try{
            Connection conn = connSQL.SetConeccion();
            
            java.sql.Statement st = conn.createStatement();
            String sql = "SELECT IdReserva From Reserva";
            
            ResultSet result = st.executeQuery(sql);
        
            while(result.next()){
            
            int id= result.getInt("IdReserva");
                        
            array.add(id);
            
            }
            result.close();
            st.close();
            conn.close();
            return array;
            
            
            }
            catch(Exception e){
                System.out.println(e);
            }
            
            return array;
    
        }
        
public ArrayList<Pago> ReturnPagos(String Documento) 
                throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException
 {
            ArrayList<Pago> array= new ArrayList();
             try{
            Connection conn = connSQL.SetConeccion();
             
            java.sql.Statement st = conn.createStatement();
            String sql = "SELECT Nombre,Documento,Totalpago,Estado,IdPagoCliente,PagoCliente.IdCliente From Cliente inner join PagoCliente on PagoCliente.IdCliente = Cliente.IdCliente Where Documento= '"+Documento+"'";
            String estado1;
            ResultSet result = st.executeQuery(sql);
            Pago pa;
            while(result.next()){
                        
            String Cliente= result.getString("Nombre");
            String Doc= result.getString("Documento");
            Float val= result.getFloat("Totalpago");            
            boolean estado= result.getBoolean("Estado");
            int idPago = result.getInt("IdPagoCliente");
            int idCliente = result.getInt("IdCliente");
            
            if(estado){
                estado1="Pagado";
            }else{
                estado1="No Pagado";
            }
            
            pa = new Pago(Cliente,idPago,idCliente,Doc,val,estado1);
            
            array.add(pa);
            
            }            
            
            result.close();
            st.close();
            conn.close();
            return array;
            
            }
            catch(Exception e){
                System.out.println(e);
            }
            return array;
 }

    public void ActualizarPagos(int idPago) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException
     {
      Connection conn = connSQL.SetConeccion();

      ps = conn.prepareStatement("UPDATE PagoCliente Set estado=true, FechaPago=current_date Where IdPagoCliente="+idPago+"");
      contador = ps.executeUpdate();
    // Cierro el resulset, statement, pstatement
      ps.close();
      conn.close();;
     }
    
 
    public void InsertarPagoCliente(int idCliente) 
                throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException
    {
                try{
      // Hago la conexi n
      Connection conn = connSQL.SetConeccion();
                    // Insertar
      ps = conn.prepareStatement("insert into PagoCliente(TotalPago,Estado,idCliente) values (0,false,"+idCliente+");");


      contador = ps.executeUpdate();



                    // Cierro el resulset, statement, pstatement
      ps.close();
                    conn.close();

                }catch(Exception e){
                    System.out.println(e);
                }   
     }
        
}
