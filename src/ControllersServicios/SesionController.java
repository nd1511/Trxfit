/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersServicios;

import Conexion.ReturnEntities;
import Entities.Sesion;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Nico
 */
public class SesionController {
    
    ReturnEntities Re = new ReturnEntities();
    Unimportant um = new Unimportant(); 
    
    public ArrayList<Sesion> FillTableSesionesPorClase(String Clase) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
    
        ArrayList<Sesion> sesiones = Re.ReturnSesion(); 
         ArrayList<Sesion> sesionesFiltradas = new ArrayList();  
        
        if(Clase.equals("Inicio")){
            
            sesionesFiltradas = Re.ReturnSesion();
            
        }else{
            
            for(Sesion ses: sesiones){
                
                if(ses.getClase().equals(Clase)){
                    
                    sesionesFiltradas.add(ses);
                }
            }
        }
        
        return sesionesFiltradas; 
    }

    public void reservar(Sesion Se) {
        
        int idCliente = um.getIdClienteActivo();
        int idSesion = Se.getIdSesion(); 
        
        try { 
            
            int idReserva = this.newIDReserva();
            
            Re.InsertarReserva(idReserva,idCliente,idSesion);
                        
        } catch (Exception ex) {
            
            System.out.println(ex);
            
        }
    }
       
    public int newIDReserva() throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException{
        
        ArrayList<Integer>idList = new ArrayList<Integer>();
        int newId;
        
        boolean repetido; 
        
        do{
        
            idList=Re.IdReserva();
              
            int newID = Integer.parseInt(UUID.randomUUID().toString().replaceAll("[^\\d]", "").substring(0, 5));
            repetido=false; 
            newId = newID;
            
            for(Integer identificacion:idList){
                if(newID==identificacion){
                   repetido=true; 
                }
            }
            
        }while(repetido);
        
        return newId;
    }

    

    
}
