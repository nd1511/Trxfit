/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexion;

import Entities.*;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.junit.Assert.assertArrayEquals;

/**
 *
 * @author Yiyisman
 */
public class ReturnEntitiesTest {
    
    public ReturnEntitiesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    @Test
    public void testReturnPagos() throws Exception {
        ReturnEntities instance = new ReturnEntities();
        
        ArrayList<Pago> array= new ArrayList();
        Pago pa=new Pago("Nicolas","1020811",100000f,"No Pagado");
        array.add(pa);
        System.out.println("ReturnPagos");
        String Documento = "1020811";
        int count=0;
        int count1=0;
        String nombrer=null;
        String nombree=null;
        ArrayList<Pago> expResult = array;
        ArrayList<Pago> result = instance.ReturnPagos(Documento);
        for(Pago p: result){
            if(count==0){
                nombrer=p.getNombre();
            }
            count++;
        }
        for(Pago p: expResult){
            if(count1==0){
                nombree=p.getNombre();
            }
            count1++;
        }

        assertEquals(nombree, nombrer);
        
    }


    
}
